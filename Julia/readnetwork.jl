#Author: Lucas Zenichi Terada
#Description: This program reads network file

#Import libraries
module readnetwork

using CSV
using DataFrames

#using this formats: http://labs.ece.uw.edu/pstca/

#IEEE Common Data Format
#PTI Power Flow Data Format
#PECO PSAP Format

a = 1;
b = [1 2 3];
export a;
export b;
end
